# translation of man-db to Turkish.
# Copyright (C) 2017 Colin Watson (msgids)
# This file is distributed under the same license as the man-db package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2017.
# Emir SARI <emir_sari@icloud.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: man-db-2.10.0-pre1\n"
"Report-Msgid-Bugs-To: Colin Watson <cjwatson@debian.org>\n"
"POT-Creation-Date: 2022-01-24 11:17+0000\n"
"PO-Revision-Date: 2022-04-11 23:00+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"

#: lib/pathsearch.c:82 lib/pathsearch.c:132 src/manp.c:1182
#, c-format
msgid "can't determine current directory"
msgstr "geçerli dizin algılanamıyor"

#: lib/security.c:81
#, c-format
msgid "can't set effective uid"
msgstr "etkili uid ayarlanamıyor"

#: lib/security.c:120
#, c-format
msgid "the setuid man user \"%s\" does not exist"
msgstr "setuid man kullanıcısı \"%s\" mevcut değil"

#: lib/xregcomp.c:50
#, c-format
msgid "fatal: regex `%s': %s"
msgstr "onulmaz: düzenli ifade `%s': %s"

#: libdb/db_delete.c:108
#, c-format
msgid "multi key %s does not exist"
msgstr "çoklu anahtar %s mevcut değil"

#: libdb/db_lookup.c:79
#, c-format
msgid "can't lock index cache %s"
msgstr "%s içerik önbelleği kilitlenemez"

#: libdb/db_lookup.c:86
#, c-format
msgid "index cache %s corrupt"
msgstr "%s içerik önbelleği bozuk"

#: libdb/db_lookup.c:92
#, c-format
msgid "cannot replace key %s"
msgstr "%s anahtarı değiştirilemez"

#: libdb/db_lookup.c:190 libdb/db_lookup.c:201
#, c-format
msgid "only %d field in content"
msgid_plural "only %d fields in content"
msgstr[0] "içerikteki sadece %d alanı"
msgstr[1] "içerikteki sadece %d alanı"

#: libdb/db_lookup.c:365
#, c-format
msgid "bad fetch on multi key %s"
msgstr "çoklu anahtar %s bozuk getirildi"

#: libdb/db_lookup.c:450 src/whatis.c:704
#, c-format
msgid "Database %s corrupted; rebuild with mandb --create"
msgstr "%s veritabanı bozuk; mandb --create ile tekrar oluşturun"

#: libdb/db_ver.c:56
#, c-format
msgid "warning: %s has no version identifier\n"
msgstr "uyarı: %s sürüm tanıtıcısı yok\n"

#: libdb/db_ver.c:60
#, c-format
msgid "warning: %s is version %s, expecting %s\n"
msgstr "uyarı: %s sürümü %s, beklenen %s\n"

#: libdb/db_ver.c:82
#, c-format
msgid "fatal: unable to insert version identifier into %s"
msgstr "onulmaz: %s içine sürüm tanıtıcısı eklenemiyor"

#: src/accessdb.c:63
msgid "[MAN DATABASE]"
msgstr "[MAN VERİTABANI]"

#: src/accessdb.c:64
#, c-format
msgid "The man database defaults to %s%s."
msgstr "Man veritabanı öntanımlısı şudur: %s%s."

#: src/accessdb.c:67 src/catman.c:104 src/globbing_test.c:62
#: src/lexgrog_test.c:79 src/man.c:277 src/man-recode.c:117
#: src/manconv_main.c:100 src/mandb.c:118 src/manpath.c:67 src/whatis.c:125
#: src/zsoelim_main.c:72
msgid "emit debugging messages"
msgstr "hata ayıklama iletileri yay"

#: src/accessdb.c:141
#, c-format
msgid "can't open %s for reading"
msgstr "%s okumak için açılamıyor"

#: src/catman.c:101
msgid "[SECTION...]"
msgstr "[BÖLÜM...]"

#: src/catman.c:105 src/man.c:298 src/whatis.c:135
msgid "PATH"
msgstr "YOL"

#: src/catman.c:105 src/man.c:298 src/whatis.c:135
msgid "set search path for manual pages to PATH"
msgstr "kılavuz sayfaları için arama yolunu YOL olarak ayarla"

#: src/catman.c:106 src/man.c:276 src/mandb.c:126 src/manpath.c:69
#: src/whatis.c:137
msgid "FILE"
msgstr "DOSYA"

#: src/catman.c:106 src/man.c:276 src/mandb.c:126 src/manpath.c:69
#: src/whatis.c:137
msgid "use this user configuration file"
msgstr "bu kullanıcı yapılandırma dosyasını kullan"

#: src/catman.c:199
#, c-format
msgid "man command failed with exit status %d"
msgstr "man komutu %d çıkış durumu ile başarısız oldu"

#: src/catman.c:239
#, c-format
msgid "cannot read database %s"
msgstr "%s veritabanı okunamıyor"

#: src/catman.c:287
#, c-format
msgid "NULL content for key: %s"
msgstr "Anahtar için boş içerik: %s"

#: src/catman.c:308
#, c-format
msgid ""
"\n"
"Updating cat files for section %s of man hierarchy %s\n"
msgstr ""
"\n"
"%2$s man hiyerarşisinin %1$s bölümü için cat dosyaları güncelleniyor\n"

#: src/catman.c:362
#, c-format
msgid "cannot write within %s"
msgstr "%s içine yazılamıyor"

#: src/catman.c:437
#, c-format
msgid "unable to update %s"
msgstr "%s güncellenemiyor"

#: src/check_mandirs.c:107
#, c-format
msgid "warning: %s/man%s/%s.%s*: competing extensions"
msgstr "uyarı: %s/man%s/%s.%s*: uzantıları çakışıyor"

#: src/check_mandirs.c:133 src/check_mandirs.c:635
#, c-format
msgid "can't update index cache %s"
msgstr "%s içerik önbelleği güncellenemedi"

#: src/check_mandirs.c:261
#, c-format
msgid "warning: %s: bad symlink or ROFF `.so' request"
msgstr "uyarı: %s: bozuk sembolik bağ veya ROFF `.so' talebi"

#: src/check_mandirs.c:317
#, c-format
msgid "warning: %s: ignoring empty file"
msgstr "uyarı: %s: boş dosya yok sayılıyor"

#: src/check_mandirs.c:321 src/straycats.c:271
#, c-format
msgid "warning: %s: whatis parse for %s(%s) failed"
msgstr "uyarı: %s: %s(%s) için whatis ayıklaması başarısız"

#: src/check_mandirs.c:349 src/check_mandirs.c:520 src/mandb.c:882
#: src/straycats.c:91 src/straycats.c:298 src/ult_src.c:84
#, c-format
msgid "can't search directory %s"
msgstr "%s dizini aranamıyor"

#: src/check_mandirs.c:398 src/man.c:1723
#, c-format
msgid "can't chown %s"
msgstr "%s üzerinde chown komutu kullanılamadı"

#: src/check_mandirs.c:420 src/check_mandirs.c:443
#, c-format
msgid "warning: cannot create catdir %s"
msgstr "uyarı: catdir %s oluşturulamıyor"

#: src/check_mandirs.c:475 src/man.c:1735 src/mandb.c:236
#, c-format
msgid "can't chmod %s"
msgstr "%s için chmod yapılamıyor"

#: src/check_mandirs.c:525
#, c-format
msgid "can't change to directory %s"
msgstr "%s dizinine değiştirilemiyor"

#: src/check_mandirs.c:575
#, c-format
msgid "can't create index cache %s"
msgstr "%s dizin önbelleği oluşturulamıyor"

#: src/check_mandirs.c:600
#, c-format
msgid "Updating index cache for path `%s/%s'. Wait..."
msgstr "`%s/%s' yolunun dizin önbelleği güncelleniyor. Bekleyin..."

#: src/check_mandirs.c:662 src/check_mandirs.c:728
msgid "done.\n"
msgstr "tamamlandı.\n"

#: src/check_mandirs.c:1001
#, c-format
msgid "Purging old database entries in %s...\n"
msgstr "%s içindeki eski veritabanı girdileri temizleniyor...\n"

#: src/descriptions_store.c:53
#, c-format
msgid "warning: failed to store entry for %s(%s)"
msgstr "uyarı: %s(%s) girdisini saklama başarısız oldu"

#: src/filenames.c:52 src/straycats.c:129 src/straycats.c:149
#, c-format
msgid "warning: %s: ignoring bogus filename"
msgstr "uyarı: %s: sahte dosya adı yok sayılıyor"

#: src/globbing_test.c:59
msgid "PATH SECTION NAME"
msgstr "YOL BÖLÜM ADI"

#: src/globbing_test.c:63 src/man.c:301
msgid "EXTENSION"
msgstr "UZANTI"

#: src/globbing_test.c:63 src/man.c:302
msgid "limit search to extension type EXTENSION"
msgstr "aramayı UZANTI uzantı türüne sınırla"

#: src/globbing_test.c:64 src/man.c:303
msgid "look for pages case-insensitively (default)"
msgstr "sayfaları harf duyarlılığı olmadan ara (öntanımlı)"

#: src/globbing_test.c:65 src/man.c:304
msgid "look for pages case-sensitively"
msgstr "sayfaları harf duyarlılığı ile ara"

#: src/globbing_test.c:66
msgid "interpret page name as a regex"
msgstr "sayfa adını düzenli ifade olarak yorumla"

#: src/globbing_test.c:67
msgid "the page name contains wildcards"
msgstr "sayfa adı özel semboller içeriyor"

#: src/lexgrog.l:725
#, c-format
msgid "warning: whatis for %s exceeds %d byte, truncating."
msgid_plural "warning: whatis for %s exceeds %d bytes, truncating."
msgstr[0] "uyarı: %s için whatis %d baytı aştı, kırpılıyor."
msgstr[1] "uyarı: %s için whatis %d baytı aştı, kırpılıyor."

#: src/lexgrog.l:887 src/man.c:2311 src/man.c:2397 src/man.c:2495
#: src/man-recode.c:183 src/man-recode.c:208 src/manconv_main.c:164
#: src/straycats.c:211 src/ult_src.c:325 src/ult_src.c:339 src/zsoelim.l:529
#, c-format
msgid "can't open %s"
msgstr "%s açılamıyor"

#: src/lexgrog_test.c:75 src/zsoelim_main.c:69
msgid "FILE..."
msgstr "DOSYA..."

#: src/lexgrog_test.c:76
msgid "The defaults are --man and --whatis."
msgstr "Öntanımlılar --man ve --whatis."

#: src/lexgrog_test.c:80
msgid "parse as man page"
msgstr "man sayfası olarak ayrıştır"

#: src/lexgrog_test.c:81
msgid "parse as cat page"
msgstr "cat sayfası olarak ayrıştır"

#: src/lexgrog_test.c:82
msgid "show whatis information"
msgstr "whatis bilgisini göster"

#: src/lexgrog_test.c:83
msgid "show guessed series of preprocessing filters"
msgstr "ön işleme filtrelerinin tahmin serilerini göster"

#: src/lexgrog_test.c:84 src/man.c:293 src/man.c:318
msgid "ENCODING"
msgstr "KODLAMA"

#: src/lexgrog_test.c:84 src/man.c:318
msgid "use selected output encoding"
msgstr "seçilen çıktı kodlamasını kullan"

#: src/lexgrog_test.c:128 src/man.c:546 src/man.c:555
#, c-format
msgid "%s: incompatible options"
msgstr "%s: uyumsuz seçenekler"

#: src/man.c:169 src/man-recode.c:242
#, c-format
msgid "command exited with status %d: %s"
msgstr "komut %d durumu ile çıktı: %s"

#: src/man.c:260
msgid "[SECTION] PAGE..."
msgstr "[BÖLÜM] SAYFA..."

#: src/man.c:278
msgid "reset all options to their default values"
msgstr "tüm seçenekleri öntanımlı değerlerine çevir"

#: src/man.c:279
msgid "WARNINGS"
msgstr "UYARILAR"

#: src/man.c:280
msgid "enable warnings from groff"
msgstr "groff uyarılarını etkinleştir"

#: src/man.c:282
msgid "Main modes of operation:"
msgstr "Ana işlem kipleri:"

#: src/man.c:283
msgid "equivalent to whatis"
msgstr "whatis ile eşdeğer"

#: src/man.c:284
msgid "equivalent to apropos"
msgstr "apropos ile eşdeğer"

#: src/man.c:285
msgid "search for text in all pages"
msgstr "metni tüm sayfalarda ara"

#: src/man.c:286
msgid "print physical location of man page(s)"
msgstr "man sayfa(larının) fiziksel konumlarını yazdır"

#: src/man.c:289
msgid "print physical location of cat file(s)"
msgstr "cat dosya(larının) fiziksel konumlarını yazdır"

#: src/man.c:291
msgid "interpret PAGE argument(s) as local filename(s)"
msgstr "DOSYA argüman(lar)ını yerel dosya(lar) olarak yorumla"

#: src/man.c:292
msgid "used by catman to reformat out of date cat pages"
msgstr ""
"eski cat sayfalarını tekrar biçimlendirmek için catman tarafından "
"kullanılıyor"

#: src/man.c:293
msgid "output source page encoded in ENCODING"
msgstr "KODLAMA olarak kodlanmış çıktı kaynak dosyası"

#: src/man.c:295
msgid "Finding manual pages:"
msgstr "Kılavuz sayfalarında arama:"

#: src/man.c:296 src/whatis.c:136
msgid "LOCALE"
msgstr "YEREL"

#: src/man.c:296
msgid "define the locale for this particular man search"
msgstr "bu özel man araması için yereli tanımla"

#: src/man.c:297 src/manpath.c:70 src/whatis.c:134
msgid "SYSTEM"
msgstr "SİSTEM"

#: src/man.c:297 src/manpath.c:70 src/whatis.c:134
msgid "use manual pages from other systems"
msgstr "diğer sistemlerden kılavuz sayfalarını kullan"

#: src/man.c:299 src/whatis.c:132
msgid "LIST"
msgstr "LİSTE"

#: src/man.c:299
msgid "use colon separated section list"
msgstr "iki nokta ile ayrılmış bölüm listesini kullan"

#: src/man.c:305
msgid "show all pages matching regex"
msgstr "düzenli ifade ile eşleşen tüm sayfaları göster"

#: src/man.c:306
msgid "show all pages matching wildcard"
msgstr "özel karakter ile eşleşen tüm sayfaları göster"

#: src/man.c:307
msgid "make --regex and --wildcard match page names only, not descriptions"
msgstr ""
"--regex ve --wildcard sadece sayfa adlarını eşleştirsin, açıklamaları değil"

#: src/man.c:309
msgid "find all matching manual pages"
msgstr "tüm eşleşen kılavuz sayfalarını bul"

#: src/man.c:310
msgid "force a cache consistency check"
msgstr "zorunlu bir önbellek düzenlilik denetimi yap"

#: src/man.c:312
msgid "don't try subpages, e.g. 'man foo bar' => 'man foo-bar'"
msgstr "alt sayfaları deneme, örn. 'man foo bar' => 'man foo-bar'"

#: src/man.c:314
msgid "Controlling formatted output:"
msgstr "Biçimlendirilmiş çıktı denetlenmesi:"

#: src/man.c:315
msgid "PAGER"
msgstr "ÇAĞRI AYGITI"

#: src/man.c:315
msgid "use program PAGER to display output"
msgstr "çıktı görüntülemek için program ÇAĞRI AYGITI kullan"

#: src/man.c:316 src/man.c:325
msgid "STRING"
msgstr "DİZİ"

#: src/man.c:316
msgid "provide the `less' pager with a prompt"
msgstr "`less' sayfalayıcıyı istek ile sun"

#: src/man.c:317
msgid "display ASCII translation of certain latin1 chars"
msgstr "bazı latin1 karakterlerinin ASCII çevirisini kullan"

#: src/man.c:320
msgid "turn off hyphenation"
msgstr "hecelemeyi kapat"

#: src/man.c:323
msgid "turn off justification"
msgstr "iki yana yaslamayı kapat"

#: src/man.c:325
msgid ""
"STRING indicates which preprocessors to run:\n"
"e - [n]eqn, p - pic, t - tbl,\n"
"g - grap, r - refer, v - vgrind"
msgstr ""
"DİZİ çalıştırılacak ön işleyicileri belirtir:\n"
"e - [n]eqn, p - pic, t - tbl,\n"
"g - grap, r - refer, v - vgrind"

#: src/man.c:329
#, c-format
msgid "use %s to format pages"
msgstr "sayfaları biçimlemek için %s kullan"

#: src/man.c:330
msgid "DEVICE"
msgstr "AYGIT"

#: src/man.c:331
#, c-format
msgid "use %s with selected device"
msgstr "seçilen aygıt ile %s kullan"

#: src/man.c:332
msgid "BROWSER"
msgstr "TARAYICI"

#: src/man.c:333
#, c-format
msgid "use %s or BROWSER to display HTML output"
msgstr "HTML çıktısını görüntülemek için %s veya TARAYICI kullan"

#: src/man.c:334
msgid "RESOLUTION"
msgstr "ÇÖZÜNÜRLÜK"

#: src/man.c:336
msgid ""
"use groff and display through gxditview (X11):\n"
"-X = -TX75, -X100 = -TX100, -X100-12 = -TX100-12"
msgstr ""
"groff kullan ve gxditview (X11) aracılığı ile görüntüle:\n"
"-X = -TX75, -X100 = -TX100, -X100-12 = -TX100-12"

#: src/man.c:338
msgid "use groff and force it to produce ditroff"
msgstr "groff kullan ve ditroff üretmesi için zorla"

#: src/man.c:605 src/man.c:742
#, c-format
msgid "No manual entry for %s\n"
msgstr "Şunun için kılavuz girdisi yok: %s\n"

#: src/man.c:607
#, c-format
msgid "(Alternatively, what manual page do you want from section %s?)\n"
msgstr "(Ek olarak, %s bölümünden hangi kılavuz sayfasını istersiniz?)\n"

#: src/man.c:611
msgid "What manual page do you want?\n"
msgstr "Hangi kılavuz sayfasını istersiniz?\n"

#: src/man.c:612
msgid "For example, try 'man man'.\n"
msgstr "Örneğin, 'man man' deneyin.\n"

#: src/man.c:739
#, c-format
msgid "No manual entry for %s in section %s\n"
msgstr "%s için %s bölümünde kılavuz girdisi yok\n"

#: src/man.c:748
#, c-format
msgid "See '%s' for help when manual pages are not available.\n"
msgstr "Kılavuz sayfaları mevcut olmadığında yardım için bkz. '%s'.\n"

#: src/man.c:1356
#, c-format
msgid "ignoring unknown preprocessor `%c'"
msgstr "bilinmeyen ön işleyici yok sayılıyor `%c'"

#: src/man.c:1746 src/man-recode.c:251 src/mandb.c:227
#, c-format
msgid "can't rename %s to %s"
msgstr "%s, %s olarak adlandırılamıyor"

#: src/man.c:1763
#, c-format
msgid "can't set times on %s"
msgstr "%s üzerinde zamanlar ayarlanamıyor"

#: src/man.c:1772 src/man.c:1809
#, c-format
msgid "can't unlink %s"
msgstr "%s bağlantısı kaldırılamıyor"

#: src/man.c:1839
#, c-format
msgid "can't create temporary cat for %s"
msgstr "%s için geçici kategori oluşturulamıyor"

#: src/man.c:1952
#, c-format
msgid "can't create temporary directory"
msgstr "geçici dizin oluşturulamıyor"

#: src/man.c:1963 src/man-recode.c:216
#, c-format
msgid "can't open temporary file %s"
msgstr "%s geçici dosyası açılamıyor"

#: src/man.c:1993 src/man.c:2022
#, c-format
msgid "can't remove directory %s"
msgstr "%s dizini kaldırılamıyor"

#: src/man.c:2153
#, c-format
msgid "--Man-- next: %s [ view (return) | skip (Ctrl-D) | quit (Ctrl-C) ]\n"
msgstr "--Man-- sonraki: %s [ bak (enter) | atla (Ctrl-D) | çık (Ctrl-C) ]\n"

#: src/man.c:2440
#, c-format
msgid ""
"\n"
"cannot write to %s in catman mode"
msgstr ""
"\n"
"catman kipinde %s içine yazılamıyor"

#: src/man.c:2520
#, c-format
msgid "Can't convert %s to cat name"
msgstr "%s cat adına dönüştürülemiyor"

#: src/man.c:3245
#, c-format
msgid "%s: relying on whatis refs is deprecated\n"
msgstr "%s: whatis refs'e dayanmak artık kullanılmıyor\n"

#: src/man.c:3388 src/man.c:4241
#, c-format
msgid "mandb command failed with exit status %d"
msgstr "mandb komutu %d çıktı durumu ile başarısız oldu"

#: src/man.c:3588
#, c-format
msgid "internal error: candidate type %d out of range"
msgstr "iç hata: aday türü %d aralık dışında"

#: src/man.c:4184
msgid " Manual page "
msgstr " Kılavuz sayfası "

#: src/man-recode.c:109
msgid "-t CODE {--suffix SUFFIX | --in-place} FILENAME..."
msgstr "-t KOD {--suffix SONEK | --in-place} DOSYAADI..."

#: src/man-recode.c:112 src/manconv_main.c:99
msgid "CODE"
msgstr "KOD"

#: src/man-recode.c:112 src/manconv_main.c:99
msgid "encoding for output"
msgstr "çıktı için kodlama"

#: src/man-recode.c:114
msgid "SUFFIX"
msgstr "SONEK"

#: src/man-recode.c:114
msgid "suffix to append to output file name"
msgstr "çıktı dosyası adına iliştirilecek sonek"

#: src/man-recode.c:116
msgid "overwrite input files in place"
msgstr "girdi dosyalarının yerinde üzerine yaz"

#: src/man-recode.c:118 src/manconv_main.c:101 src/manpath.c:68
msgid "produce fewer warnings"
msgstr "daha az uyarı üret"

#: src/man-recode.c:154 src/manconv_main.c:136
#, c-format
msgid "must specify an output encoding"
msgstr "bir çıktı kodlaması belirtilmeli"

#: src/man-recode.c:158
#, c-format
msgid "must use either --suffix or --in-place"
msgstr "ya --suffix ya da --in-place kullanmalı"

#: src/man-recode.c:162
#, c-format
msgid "--suffix and --in-place are mutually exclusive"
msgstr "--suffix ve --in-place birlikte kullanılamaz"

#: src/man-recode.c:257 src/mandb.c:220
#, c-format
msgid "can't remove %s"
msgstr "%s kaldırılamıyor"

#: src/manconv.c:237
#, c-format
msgid "can't write to standard output"
msgstr "standart çıktıya yazılamıyor"

#: src/manconv.c:466
msgid "iconv: incomplete character at end of buffer"
msgstr "iconv: tampon sonunda eksik karakter"

#: src/manconv_main.c:94
msgid "[-f CODE[:...]] -t CODE [FILENAME]"
msgstr "[-f KOD[:...]] -t KOD [DOSYAADI]"

#: src/manconv_main.c:97
msgid "CODE[:...]"
msgstr "KOD[:...]"

#: src/manconv_main.c:98
msgid "possible encodings of original text"
msgstr "özgün metnin muhtemel kodlamaları"

#: src/mandb.c:115
msgid "[MANPATH]"
msgstr "[MAN YOLU]"

#: src/mandb.c:119
msgid "work quietly, except for 'bogus' warning"
msgstr "'sahte' uyarılar hariç sessiz çalış"

#: src/mandb.c:120
msgid "don't look for or add stray cats to the dbs"
msgstr "veritabanlarında stray cat arama veya ekleme"

#: src/mandb.c:121
msgid "don't purge obsolete entries from the dbs"
msgstr "veritabanlarından gereksiz girdileri budama"

#: src/mandb.c:122
msgid "produce user databases only"
msgstr "sadece kullanıcı veritabanlarını üret"

#: src/mandb.c:123
msgid "create dbs from scratch, rather than updating"
msgstr "veritabanlarını güncellemek yerine sıfırdan üret"

#: src/mandb.c:124
msgid "check manual pages for correctness"
msgstr "doğrulukları için kılavuz sayfalarını denetle"

#: src/mandb.c:125
msgid "FILENAME"
msgstr "DOSYA ADI"

#: src/mandb.c:125
msgid "update just the entry for this filename"
msgstr "bu dosya adı için sadece girdiyi güncelle"

#: src/mandb.c:284
#, c-format
msgid "can't write to %s"
msgstr "%s ögesine yazılamıyor"

#: src/mandb.c:289
#, c-format
msgid "can't read from %s"
msgstr "%s ögesinden okunamıyor"

#: src/mandb.c:461
#, c-format
msgid "Processing manual pages under %s...\n"
msgstr "%s altındaki kılavuz sayfaları işleniyor...\n"

#: src/mandb.c:671 src/mandb.c:699
#, c-format
msgid "Removing obsolete cat directory %s...\n"
msgstr "%s gereksiz cat dizini kaldırılıyor...\n"

#: src/mandb.c:849
#, c-format
msgid "warning: no MANDB_MAP directives in %s, using your manpath"
msgstr ""
"uyarı: %s içinde MANDB_MAP yönergesi yok, yazdığınız manpath kullanılıyor"

#: src/mandb.c:917
#, c-format
msgid "%d man subdirectory contained newer manual pages.\n"
msgid_plural "%d man subdirectories contained newer manual pages.\n"
msgstr[0] "%d man alt dizini daha yeni kılavuz sayfaları içeriyor.\n"
msgstr[1] "%d man alt dizini daha yeni kılavuz sayfaları içeriyor.\n"

#: src/mandb.c:922
#, c-format
msgid "%d manual page was added.\n"
msgid_plural "%d manual pages were added.\n"
msgstr[0] "%d kılavuz sayfası eklendi.\n"
msgstr[1] "%d kılavuz sayfası eklendi.\n"

#: src/mandb.c:926
#, c-format
msgid "%d stray cat was added.\n"
msgid_plural "%d stray cats were added.\n"
msgstr[0] "%d başıboş cat eklendi.\n"
msgstr[1] "%d başıboş cat eklendi.\n"

#: src/mandb.c:931
#, c-format
msgid "%d old database entry was purged.\n"
msgid_plural "%d old database entries were purged.\n"
msgstr[0] "%d eski veritabanı girdisi budandı.\n"
msgstr[1] "%d eski veritabanı girdisi budandı.\n"

#: src/mandb.c:949
#, c-format
msgid "No databases created."
msgstr "Bir veritabanı oluşturulmadı."

#: src/manp.c:327
#, c-format
msgid "can't make sense of the manpath configuration file %s"
msgstr "%s manpath yapılandırma dosyası anlaşılamıyor"

#: src/manp.c:333
#, c-format
msgid "warning: %s"
msgstr "uyarı: %s"

#: src/manp.c:339
#, c-format
msgid "warning: %s isn't a directory"
msgstr "uyarı: %s bir dizin değil"

#: src/manp.c:670
#, c-format
msgid "warning: $PATH not set"
msgstr "uyarı: $PATH ayarlanmamış"

#: src/manp.c:677
#, c-format
msgid "warning: empty $PATH"
msgstr "uyarı: boş $PATH"

#: src/manp.c:705
#, c-format
msgid "warning: $MANPATH set, prepending %s"
msgstr "uyarı: $MANPATH ayarlanmış, %s öne ekleniyor"

#: src/manp.c:716
#, c-format
msgid "warning: $MANPATH set, appending %s"
msgstr "uyarı: $MANPATH ayarlanmış, %s sona ekleniyor"

#: src/manp.c:728
#, c-format
msgid "warning: $MANPATH set, inserting %s"
msgstr "uyarı: $MANPATH ayarlanmış, %s ekleniyor"

#: src/manp.c:742
#, c-format
msgid "warning: $MANPATH set, ignoring %s"
msgstr "uyarı: $MANPATH ayarlanmış, %s yok sayılıyor"

#: src/manp.c:804
#, c-format
msgid "can't parse directory list `%s'"
msgstr "`%s' dizin listesi ayrıştırılamıyor"

#: src/manp.c:859
#, c-format
msgid "can't open the manpath configuration file %s"
msgstr "%s manpath yapılandırma dosyası açılamıyor"

#: src/manp.c:896
#, c-format
msgid "warning: mandatory directory %s doesn't exist"
msgstr "uyarı: zorunlu dizin %s mevcut değil"

#: src/manp.c:1359
#, c-format
msgid "warning: %s does not begin with %s"
msgstr "uyarı: %s, %s ile başlamıyor"

#: src/manpath.c:65
msgid "show relative catpaths"
msgstr "ilişkili catpath'leri göster"

#: src/manpath.c:66
msgid "show the entire global manpath"
msgstr "tüm global manpath'i göster"

#: src/manpath.c:128
#, c-format
msgid "warning: no global manpaths set in config file %s"
msgstr "uyarı: %s yapılandırma dosyasında ayarlanmış global bir manpath yok"

#: src/straycats.c:241 src/ult_src.c:128
#, c-format
msgid "warning: %s is a dangling symlink"
msgstr "uyarı: %s sarkan bir sembolik bağ"

#: src/straycats.c:244 src/ult_src.c:131 src/ult_src.c:267
#, c-format
msgid "can't resolve %s"
msgstr "%s çözülemiyor"

#: src/straycats.c:303
#, c-format
msgid "Checking for stray cats under %s...\n"
msgstr "%s altında başıboş cat'ler aranıyor...\n"

#: src/straycats.c:343
#, c-format
msgid "warning: can't update index cache %s"
msgstr "uyarı: %s dizin önbelleği güncellenemiyor"

#: src/ult_src.c:305
#, c-format
msgid "%s is self referencing"
msgstr "%s kendine başvuruyor"

#: src/whatis.c:121
msgid "KEYWORD..."
msgstr "ANAHTAR SÖZCÜK..."

#: src/whatis.c:122
msgid "The --regex option is enabled by default."
msgstr "--regex seçeneği öntanımlı olarak etkindir."

#: src/whatis.c:126
msgid "print verbose warning messages"
msgstr "ayrıntılı uyarı iletilerini göster"

#: src/whatis.c:127
msgid "interpret each keyword as a regex"
msgstr "her anahtar sözcüğü düzenli ifade olarak yorumla"

#: src/whatis.c:128
msgid "search each keyword for exact match"
msgstr "her anahtar sözcüğü tam eşleşme ile ara"

#: src/whatis.c:129
msgid "the keyword(s) contain wildcards"
msgstr "anahtar sözcük(ler) özel sembol içeriyor"

#: src/whatis.c:130
msgid "require all keywords to match"
msgstr "tüm anahtar sözcüklerin eşleşmesini gerektir"

#: src/whatis.c:131
msgid "do not trim output to terminal width"
msgstr "çıktıyı uçbirim genişliğine kısıtlama"

#: src/whatis.c:132
msgid "search only these sections (colon-separated)"
msgstr "sadece şu alanlarda ara (iki nokta ile ayrılmış)"

#: src/whatis.c:136
msgid "define the locale for this search"
msgstr "bu arama için dili tanımla"

#: src/whatis.c:233
#, c-format
msgid "%s what?\n"
msgstr "%s ne?\n"

#: src/whatis.c:377 src/whatis.c:395
#, c-format
msgid "warning: %s contains a pointer loop"
msgstr "uyarı: %s bir işaretçi döngüsü içeriyor"

#: src/whatis.c:389 src/whatis.c:397
msgid "(unknown subject)"
msgstr "(bilinmeyen konu)"

#: src/whatis.c:836
#, c-format
msgid "%s: nothing appropriate.\n"
msgstr "%s: uygun bir şey yok.\n"

#: src/zsoelim.l:183
#, c-format
msgid "%s:%d: .so requests nested too deeply or are recursive"
msgstr "%s:%d: .so istekleri çok derin yuvalanmış veya içiçe çağrışım yapıyor"

#: src/zsoelim.l:198
#, c-format
msgid "%s:%d: warning: failed .so request"
msgstr "%s:%d: uyarı: başarısız .so isteği"

#: src/zsoelim.l:220
#, c-format
msgid "%s:%d: warning: newline in .so request, ignoring"
msgstr "%s:%d: uyarı: .so isteğinde yeni satır, yok sayılıyor"

#: src/zsoelim.l:290
#, c-format
msgid "%s:%d: warning: newline in .lf request, ignoring"
msgstr "%s:%d: uyarı: .lf isteği içinde yeni satır, yok sayılıyor"

#: src/zsoelim.l:330
#, c-format
msgid "%s:%d: unterminated quote in roff request"
msgstr "%s:%d: roff isteğinde kapatılmamış kesme işareti"

#: src/zsoelim_main.c:73
msgid "compatibility switch (ignored)"
msgstr "uyumluluk anahtarı (yok sayıldı)"
