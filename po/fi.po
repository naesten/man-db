# Finnish translation of man-db.
# Copyright © 2015 Colin Watson (msgids)
# This file is distributed under the same license as the man-db package.
# Lauri Nurmi <lanurmi@iki.fi>, 2003, 2004, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: man-db 2.7.0-pre1\n"
"Report-Msgid-Bugs-To: Colin Watson <cjwatson@debian.org>\n"
"POT-Creation-Date: 2022-03-17 18:26+0000\n"
"PO-Revision-Date: 2015-07-28 21:52+0300\n"
"Last-Translator: Lauri Nurmi <lanurmi@iki.fi>\n"
"Language-Team: Finnish <translation-team-fi@lists.sourceforge.net>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.3\n"

#: lib/pathsearch.c:83 lib/pathsearch.c:133 src/manp.c:1154
#, c-format
msgid "can't determine current directory"
msgstr "nykyistä hakemistoa ei voi määrittää"

#: lib/security.c:80
#, c-format
msgid "can't set effective uid"
msgstr "voimassaolevaa UID:ta ei voi asettaa"

#: lib/security.c:119
#, c-format
msgid "the setuid man user \"%s\" does not exist"
msgstr ""

#: lib/xregcomp.c:50
#, c-format
msgid "fatal: regex `%s': %s"
msgstr ""

#: libdb/db_delete.c:108
#, c-format
msgid "multi key %s does not exist"
msgstr ""

#: libdb/db_lookup.c:80
#, c-format
msgid "can't lock index cache %s"
msgstr ""

#: libdb/db_lookup.c:87
#, c-format
msgid "index cache %s corrupt"
msgstr ""

#: libdb/db_lookup.c:93
#, c-format
msgid "cannot replace key %s"
msgstr ""

#: libdb/db_lookup.c:191 libdb/db_lookup.c:202
#, c-format
msgid "only %d field in content"
msgid_plural "only %d fields in content"
msgstr[0] ""
msgstr[1] ""

#: libdb/db_lookup.c:366
#, c-format
msgid "bad fetch on multi key %s"
msgstr ""

#: libdb/db_lookup.c:451 src/whatis.c:705
#, c-format
msgid "Database %s corrupted; rebuild with mandb --create"
msgstr ""
"Tietokanta %s on turmeltunut; luo se uudelleen komennolla mandb --create"

#: libdb/db_ver.c:56
#, c-format
msgid "warning: %s has no version identifier\n"
msgstr ""

#: libdb/db_ver.c:60
#, c-format
msgid "warning: %s is version %s, expecting %s\n"
msgstr ""

#: libdb/db_ver.c:82
#, c-format
msgid "fatal: unable to insert version identifier into %s"
msgstr ""

#: src/accessdb.c:62
msgid "[MAN DATABASE]"
msgstr ""

#: src/accessdb.c:63
#, c-format
msgid "The man database defaults to %s%s."
msgstr ""

#: src/accessdb.c:66 src/catman.c:105 src/globbing_test.c:62
#: src/lexgrog_test.c:79 src/man.c:277 src/man-recode.c:118
#: src/manconv_main.c:100 src/mandb.c:118 src/manpath.c:67 src/whatis.c:126
#: src/zsoelim_main.c:72
msgid "emit debugging messages"
msgstr ""

#: src/accessdb.c:140
#, c-format
msgid "can't open %s for reading"
msgstr ""

#: src/catman.c:102
msgid "[SECTION...]"
msgstr "[OSIO...]"

#: src/catman.c:106 src/man.c:298 src/whatis.c:136
msgid "PATH"
msgstr "POLKU"

#: src/catman.c:106 src/man.c:298 src/whatis.c:136
msgid "set search path for manual pages to PATH"
msgstr "aseta opastesivujen hakupoluksi POLKU"

#: src/catman.c:107 src/man.c:276 src/mandb.c:126 src/manpath.c:69
#: src/whatis.c:138
msgid "FILE"
msgstr "TIEDOSTO"

#: src/catman.c:107 src/man.c:276 src/mandb.c:126 src/manpath.c:69
#: src/whatis.c:138
#, fuzzy
msgid "use this user configuration file"
msgstr "opastepolkujen asetustiedostoa %s ei voi avata"

#: src/catman.c:199
#, c-format
msgid "man command failed with exit status %d"
msgstr ""

#: src/catman.c:275
#, c-format
msgid "NULL content for key: %s"
msgstr ""

#: src/catman.c:296
#, c-format
msgid ""
"\n"
"Updating cat files for section %s of man hierarchy %s\n"
msgstr ""

#: src/catman.c:348
#, c-format
msgid "cannot write within %s"
msgstr ""

#: src/catman.c:415
#, c-format
msgid "cannot read database %s"
msgstr "tietokantaa %s ei voi lukea"

#: src/catman.c:431
#, c-format
msgid "unable to update %s"
msgstr ""

#: src/check_mandirs.c:108
#, c-format
msgid "warning: %s/man%s/%s.%s*: competing extensions"
msgstr ""

#: src/check_mandirs.c:134
#, c-format
msgid "can't update index cache %s"
msgstr ""

#: src/check_mandirs.c:273
#, c-format
msgid "warning: %s: bad symlink or ROFF `.so' request"
msgstr ""

#: src/check_mandirs.c:329
#, c-format
msgid "warning: %s: ignoring empty file"
msgstr "varoitus: %s: ei huomioida tyhjää tiedostoa"

#: src/check_mandirs.c:333 src/straycats.c:272
#, c-format
msgid "warning: %s: whatis parse for %s(%s) failed"
msgstr ""

#: src/check_mandirs.c:361 src/check_mandirs.c:531 src/mandb.c:891
#: src/straycats.c:92 src/straycats.c:299 src/ult_src.c:84
#, c-format
msgid "can't search directory %s"
msgstr ""

#: src/check_mandirs.c:410 src/man.c:1726
#, c-format
msgid "can't chown %s"
msgstr ""

#: src/check_mandirs.c:432 src/check_mandirs.c:455
#, c-format
msgid "warning: cannot create catdir %s"
msgstr ""

#: src/check_mandirs.c:487 src/man.c:1738 src/mandb.c:236
#, c-format
msgid "can't chmod %s"
msgstr ""

#: src/check_mandirs.c:536
#, c-format
msgid "can't change to directory %s"
msgstr ""

#: src/check_mandirs.c:584
#, c-format
msgid "can't create index cache %s"
msgstr ""

#: src/check_mandirs.c:605
#, c-format
msgid "Updating index cache for path `%s/%s'. Wait..."
msgstr ""

#: src/check_mandirs.c:631 src/check_mandirs.c:687
msgid "done.\n"
msgstr "valmis.\n"

#: src/check_mandirs.c:956
#, c-format
msgid "Purging old database entries in %s...\n"
msgstr ""

#: src/descriptions_store.c:58
#, c-format
msgid "warning: failed to store entry for %s(%s)"
msgstr ""

#: src/filenames.c:52 src/straycats.c:130 src/straycats.c:150
#, c-format
msgid "warning: %s: ignoring bogus filename"
msgstr ""

#: src/globbing_test.c:59
msgid "PATH SECTION NAME"
msgstr ""

#: src/globbing_test.c:63 src/man.c:301
msgid "EXTENSION"
msgstr ""

#: src/globbing_test.c:63 src/man.c:302
msgid "limit search to extension type EXTENSION"
msgstr ""

#: src/globbing_test.c:64 src/man.c:303
msgid "look for pages case-insensitively (default)"
msgstr ""

#: src/globbing_test.c:65 src/man.c:304
msgid "look for pages case-sensitively"
msgstr ""

#: src/globbing_test.c:66
msgid "interpret page name as a regex"
msgstr ""

#: src/globbing_test.c:67
msgid "the page name contains wildcards"
msgstr "sivun nimi sisältää jokerimerkkejä"

#: src/lexgrog.l:725
#, c-format
msgid "warning: whatis for %s exceeds %d byte, truncating."
msgid_plural "warning: whatis for %s exceeds %d bytes, truncating."
msgstr[0] ""
msgstr[1] ""

#: src/lexgrog.l:887 src/man.c:2315 src/man.c:2401 src/man.c:2499
#: src/man-recode.c:184 src/man-recode.c:209 src/manconv_main.c:164
#: src/straycats.c:212 src/ult_src.c:325 src/ult_src.c:339 src/zsoelim.l:529
#, c-format
msgid "can't open %s"
msgstr ""

#: src/lexgrog_test.c:75 src/zsoelim_main.c:69
msgid "FILE..."
msgstr "TIEDOSTO..."

#: src/lexgrog_test.c:76
msgid "The defaults are --man and --whatis."
msgstr ""

#: src/lexgrog_test.c:80
msgid "parse as man page"
msgstr "jäsennä man-sivuna"

#: src/lexgrog_test.c:81
msgid "parse as cat page"
msgstr "jäsennä cat-sivuna"

#: src/lexgrog_test.c:82
msgid "show whatis information"
msgstr "näytä whatis-tiedot"

#: src/lexgrog_test.c:83
msgid "show guessed series of preprocessing filters"
msgstr "näytä arvattu sarja esikäsittelysuotimia"

#: src/lexgrog_test.c:84 src/man.c:293 src/man.c:318
msgid "ENCODING"
msgstr "MERKISTÖ"

#: src/lexgrog_test.c:84 src/man.c:318
msgid "use selected output encoding"
msgstr "käytä valittua tulosteen merkistöä"

#: src/lexgrog_test.c:128 src/man.c:549 src/man.c:558
#, fuzzy, c-format
msgid "%s: incompatible options"
msgstr ": epäyhteensopivat valitsimet"

#: src/man.c:170 src/man-recode.c:242
#, c-format
msgid "command exited with status %d: %s"
msgstr ""

#: src/man.c:260
msgid "[SECTION] PAGE..."
msgstr "[OSIO] SIVU..."

#: src/man.c:278
msgid "reset all options to their default values"
msgstr "palauta kaikki valitsimet oletusarvoihinsa"

#: src/man.c:279
msgid "WARNINGS"
msgstr "VAROITUKSET"

#: src/man.c:280
msgid "enable warnings from groff"
msgstr ""

#: src/man.c:282
msgid "Main modes of operation:"
msgstr "Päätoimintatilat:"

#: src/man.c:283
msgid "equivalent to whatis"
msgstr "whatis-vastine"

#: src/man.c:284
msgid "equivalent to apropos"
msgstr "apropos-vastine"

#: src/man.c:285
msgid "search for text in all pages"
msgstr "etsi tekstiä kaikilta sivuilta"

#: src/man.c:286
msgid "print physical location of man page(s)"
msgstr "näytä opastesivu(je)n fyysinen sijainti"

#: src/man.c:289
msgid "print physical location of cat file(s)"
msgstr "näytä cat-sivu(je)n fyysinen sijainti"

#: src/man.c:291
msgid "interpret PAGE argument(s) as local filename(s)"
msgstr "tulkitse SIVU-argumentti paikallisena tiedostonimenä"

#: src/man.c:292
msgid "used by catman to reformat out of date cat pages"
msgstr ""

#: src/man.c:293
msgid "output source page encoded in ENCODING"
msgstr ""

#: src/man.c:295
#, fuzzy
msgid "Finding manual pages:"
msgstr " Opastesivu "

#: src/man.c:296 src/whatis.c:137
msgid "LOCALE"
msgstr ""

#: src/man.c:296
msgid "define the locale for this particular man search"
msgstr ""

#: src/man.c:297 src/manpath.c:70 src/whatis.c:135
msgid "SYSTEM"
msgstr "JÄRJESTELMÄ"

#: src/man.c:297 src/manpath.c:70 src/whatis.c:135
msgid "use manual pages from other systems"
msgstr ""

#: src/man.c:299 src/whatis.c:133
msgid "LIST"
msgstr ""

#: src/man.c:299
msgid "use colon separated section list"
msgstr ""

#: src/man.c:305
msgid "show all pages matching regex"
msgstr ""

#: src/man.c:306
msgid "show all pages matching wildcard"
msgstr ""

#: src/man.c:307
msgid "make --regex and --wildcard match page names only, not descriptions"
msgstr ""

#: src/man.c:309
msgid "find all matching manual pages"
msgstr ""

#: src/man.c:310
msgid "force a cache consistency check"
msgstr ""

#: src/man.c:312
msgid "don't try subpages, e.g. 'man foo bar' => 'man foo-bar'"
msgstr ""

#: src/man.c:314
msgid "Controlling formatted output:"
msgstr "Muotoillun tulosteen hallinta:"

#: src/man.c:315
msgid "PAGER"
msgstr ""

#: src/man.c:315
msgid "use program PAGER to display output"
msgstr ""

#: src/man.c:316 src/man.c:325
msgid "STRING"
msgstr "MERKKIJONO"

#: src/man.c:316
msgid "provide the `less' pager with a prompt"
msgstr ""

#: src/man.c:317
msgid "display ASCII translation of certain latin1 chars"
msgstr ""

#: src/man.c:320
msgid "turn off hyphenation"
msgstr ""

#: src/man.c:323
msgid "turn off justification"
msgstr ""

#: src/man.c:325
msgid ""
"STRING indicates which preprocessors to run:\n"
"e - [n]eqn, p - pic, t - tbl,\n"
"g - grap, r - refer, v - vgrind"
msgstr ""

#: src/man.c:329
#, c-format
msgid "use %s to format pages"
msgstr ""

#: src/man.c:330
msgid "DEVICE"
msgstr "LAITE"

#: src/man.c:331
#, c-format
msgid "use %s with selected device"
msgstr ""

#: src/man.c:332
msgid "BROWSER"
msgstr "SELAIN"

#: src/man.c:333
#, c-format
msgid "use %s or BROWSER to display HTML output"
msgstr ""

#: src/man.c:334
msgid "RESOLUTION"
msgstr "RESOLUUTIO"

#: src/man.c:336
msgid ""
"use groff and display through gxditview (X11):\n"
"-X = -TX75, -X100 = -TX100, -X100-12 = -TX100-12"
msgstr ""

#: src/man.c:338
msgid "use groff and force it to produce ditroff"
msgstr ""

#: src/man.c:608 src/man.c:745
#, fuzzy, c-format
msgid "No manual entry for %s\n"
msgstr "Sovellukselle %s ei ole opastesivua"

#: src/man.c:610
#, fuzzy, c-format
msgid "(Alternatively, what manual page do you want from section %s?)\n"
msgstr "Minkä opastesivun haluat osiosta %s?\n"

#: src/man.c:614
msgid "What manual page do you want?\n"
msgstr "Minkä opastesivun haluat?\n"

#: src/man.c:615
msgid "For example, try 'man man'.\n"
msgstr ""

#: src/man.c:742
#, fuzzy, c-format
msgid "No manual entry for %s in section %s\n"
msgstr "Sovellukselle %s ei ole opastesivua"

#: src/man.c:751
#, c-format
msgid "See '%s' for help when manual pages are not available.\n"
msgstr ""

#: src/man.c:1359
#, c-format
msgid "ignoring unknown preprocessor `%c'"
msgstr "ei huomioida tuntematonta esikäsittelintä \"%c\""

#: src/man.c:1749 src/man-recode.c:251 src/mandb.c:227
#, c-format
msgid "can't rename %s to %s"
msgstr ""

#: src/man.c:1766
#, c-format
msgid "can't set times on %s"
msgstr ""

#: src/man.c:1775 src/man.c:1812
#, c-format
msgid "can't unlink %s"
msgstr ""

#: src/man.c:1842
#, fuzzy, c-format
msgid "can't create temporary cat for %s"
msgstr "varoitus: väliaikaistiedostoa %s ei voi luoda"

#: src/man.c:1954
#, fuzzy, c-format
msgid "can't create temporary directory"
msgstr "nykyistä hakemistoa ei voi määrittää"

#: src/man.c:1965 src/man-recode.c:217
#, fuzzy, c-format
msgid "can't open temporary file %s"
msgstr "opastepolkujen asetustiedostoa %s ei voi avata"

#: src/man.c:1995 src/man.c:2026
#, c-format
msgid "can't remove directory %s"
msgstr ""

#: src/man.c:2157
#, c-format
msgid "--Man-- next: %s [ view (return) | skip (Ctrl-D) | quit (Ctrl-C) ]\n"
msgstr ""

#: src/man.c:2444
#, c-format
msgid ""
"\n"
"cannot write to %s in catman mode"
msgstr ""

#: src/man.c:2524
#, c-format
msgid "Can't convert %s to cat name"
msgstr ""

#: src/man.c:3296
#, c-format
msgid "%s: relying on whatis refs is deprecated\n"
msgstr ""

#: src/man.c:3439 src/man.c:4297
#, c-format
msgid "mandb command failed with exit status %d"
msgstr "mandb-komento epäonnistui paluuarvolla %d"

#: src/man.c:3637
#, c-format
msgid "internal error: candidate type %d out of range"
msgstr ""

#: src/man.c:4240
msgid " Manual page "
msgstr " Opastesivu "

#: src/man-recode.c:110
msgid "-t CODE {--suffix SUFFIX | --in-place} FILENAME..."
msgstr ""

#: src/man-recode.c:113 src/manconv_main.c:99
msgid "CODE"
msgstr ""

#: src/man-recode.c:113 src/manconv_main.c:99
msgid "encoding for output"
msgstr "tulosteen merkistö"

#: src/man-recode.c:115
msgid "SUFFIX"
msgstr ""

#: src/man-recode.c:115
msgid "suffix to append to output file name"
msgstr ""

#: src/man-recode.c:117
msgid "overwrite input files in place"
msgstr ""

#: src/man-recode.c:119 src/manconv_main.c:101 src/manpath.c:68
msgid "produce fewer warnings"
msgstr "tuota vähemmän varoituksia"

#: src/man-recode.c:155 src/manconv_main.c:136
#, c-format
msgid "must specify an output encoding"
msgstr "tulosteen merkistö on annettava"

#: src/man-recode.c:159
#, c-format
msgid "must use either --suffix or --in-place"
msgstr ""

#: src/man-recode.c:163
#, c-format
msgid "--suffix and --in-place are mutually exclusive"
msgstr ""

#: src/man-recode.c:257 src/mandb.c:220
#, c-format
msgid "can't remove %s"
msgstr "tiedostoa %s ei voi poistaa"

#: src/manconv.c:238
#, fuzzy, c-format
msgid "can't write to standard output"
msgstr "tiedostoon %s ei voi kirjoittaa"

#: src/manconv.c:467
msgid "iconv: incomplete character at end of buffer"
msgstr "iconv: epätäydellinen merkki puskurin lopussa"

#: src/manconv_main.c:94
msgid "[-f CODE[:...]] -t CODE [FILENAME]"
msgstr ""

#: src/manconv_main.c:97
msgid "CODE[:...]"
msgstr ""

#: src/manconv_main.c:98
msgid "possible encodings of original text"
msgstr "alkuperäisen tekstin mahdolliset merkistöt"

#: src/mandb.c:115
msgid "[MANPATH]"
msgstr ""

#: src/mandb.c:119
msgid "work quietly, except for 'bogus' warning"
msgstr ""

#: src/mandb.c:120
msgid "don't look for or add stray cats to the dbs"
msgstr ""

#: src/mandb.c:121
msgid "don't purge obsolete entries from the dbs"
msgstr ""

#: src/mandb.c:122
msgid "produce user databases only"
msgstr ""

#: src/mandb.c:123
msgid "create dbs from scratch, rather than updating"
msgstr ""

#: src/mandb.c:124
msgid "check manual pages for correctness"
msgstr ""

#: src/mandb.c:125
msgid "FILENAME"
msgstr "TIEDOSTONIMI"

#: src/mandb.c:125
msgid "update just the entry for this filename"
msgstr ""

#: src/mandb.c:284
#, c-format
msgid "can't write to %s"
msgstr "tiedostoon %s ei voi kirjoittaa"

#: src/mandb.c:289
#, c-format
msgid "can't read from %s"
msgstr "tiedostosta %s ei voi lukea"

#: src/mandb.c:560
#, c-format
msgid "Processing manual pages under %s...\n"
msgstr "Käsitellään opastesivuja hakemistossa %s...\n"

#: src/mandb.c:680 src/mandb.c:708
#, c-format
msgid "Removing obsolete cat directory %s...\n"
msgstr "Poistetaan vanhentunut cat-hakemisto %s...\n"

#: src/mandb.c:858
#, c-format
msgid "warning: no MANDB_MAP directives in %s, using your manpath"
msgstr ""

#: src/mandb.c:926
#, c-format
msgid "%d man subdirectory contained newer manual pages.\n"
msgid_plural "%d man subdirectories contained newer manual pages.\n"
msgstr[0] "%d man-alihakemisto sisälsi uudempia opastesivuja.\n"
msgstr[1] "%d man-alihakemistoa sisälsi uudempia opastesivuja.\n"

#: src/mandb.c:931
#, fuzzy, c-format
msgid "%d manual page was added.\n"
msgid_plural "%d manual pages were added.\n"
msgstr[0] " Opastesivu "
msgstr[1] " Opastesivu "

#: src/mandb.c:935
#, fuzzy, c-format
msgid "%d stray cat was added.\n"
msgid_plural "%d stray cats were added.\n"
msgstr[0] "%d jotain .. ööö.. kulkukissaa lisättiin.\n"
msgstr[1] "%d jotain .. ööö.. kulkukissaa lisättiin.\n"

#: src/mandb.c:940
#, fuzzy, c-format
msgid "%d old database entry was purged.\n"
msgid_plural "%d old database entries were purged.\n"
msgstr[0] "%d vanhaa tietokantamerkintää poistettiin.\n"
msgstr[1] "%d vanhaa tietokantamerkintää poistettiin.\n"

#: src/mandb.c:958
#, c-format
msgid "No databases created."
msgstr "Tietokantoja ei luotu."

#: src/manp.c:328
#, c-format
msgid "can't make sense of the manpath configuration file %s"
msgstr ""

#: src/manp.c:334
#, c-format
msgid "warning: %s"
msgstr "varoitus: %s"

#: src/manp.c:340
#, c-format
msgid "warning: %s isn't a directory"
msgstr "varoitus: %s ei ole hakemisto"

#: src/manp.c:671
#, c-format
msgid "warning: $PATH not set"
msgstr "varoitus: polkumuuttujaa $PATH ei ole asetettu"

#: src/manp.c:678
#, c-format
msgid "warning: empty $PATH"
msgstr "varoitus: tyhjä polkumuuttuja $PATH"

#: src/manp.c:706
#, c-format
msgid "warning: $MANPATH set, prepending %s"
msgstr ""

#: src/manp.c:717
#, c-format
msgid "warning: $MANPATH set, appending %s"
msgstr "varoitus: $MANPATH asetettu, lisätään loppuun %s"

#: src/manp.c:729
#, c-format
msgid "warning: $MANPATH set, inserting %s"
msgstr ""

#: src/manp.c:743
#, c-format
msgid "warning: $MANPATH set, ignoring %s"
msgstr ""

#: src/manp.c:805
#, c-format
msgid "can't parse directory list `%s'"
msgstr ""

#: src/manp.c:860
#, c-format
msgid "can't open the manpath configuration file %s"
msgstr "opastepolkujen asetustiedostoa %s ei voi avata"

#: src/manp.c:897
#, c-format
msgid "warning: mandatory directory %s doesn't exist"
msgstr "varoitus: välttämätön hakemisto %s ei ole olemassa"

#: src/manp.c:1341
#, c-format
msgid "warning: %s does not begin with %s"
msgstr "varoitus: %s ei ala merkkijonolla %s"

#: src/manpath.c:65
msgid "show relative catpaths"
msgstr ""

#: src/manpath.c:66
msgid "show the entire global manpath"
msgstr ""

#: src/manpath.c:128
#, c-format
msgid "warning: no global manpaths set in config file %s"
msgstr ""
"varoitus: asetustiedostossa %s ei ole asetettu järjestelmänlaajuisia "
"opastepolkuja"

#: src/straycats.c:242 src/ult_src.c:128
#, c-format
msgid "warning: %s is a dangling symlink"
msgstr "varoitus: %s on rikkinäinen symlinkki"

#: src/straycats.c:245 src/ult_src.c:131 src/ult_src.c:267
#, c-format
msgid "can't resolve %s"
msgstr ""

#: src/straycats.c:304
#, c-format
msgid "Checking for stray cats under %s...\n"
msgstr ""

#: src/ult_src.c:305
#, c-format
msgid "%s is self referencing"
msgstr "%s viittaa itseensä"

#: src/whatis.c:122
msgid "KEYWORD..."
msgstr "AVAINSANA..."

#: src/whatis.c:123
msgid "The --regex option is enabled by default."
msgstr ""

#: src/whatis.c:127
msgid "print verbose warning messages"
msgstr ""

#: src/whatis.c:128
msgid "interpret each keyword as a regex"
msgstr ""

#: src/whatis.c:129
msgid "search each keyword for exact match"
msgstr ""

#: src/whatis.c:130
msgid "the keyword(s) contain wildcards"
msgstr "avainsana(t) sisältävät jokerimerkkejä"

#: src/whatis.c:131
msgid "require all keywords to match"
msgstr ""

#: src/whatis.c:132
msgid "do not trim output to terminal width"
msgstr "älä katkaise tulostetta päätteen leveyteen"

#: src/whatis.c:133
msgid "search only these sections (colon-separated)"
msgstr ""

#: src/whatis.c:137
msgid "define the locale for this search"
msgstr ""

#: src/whatis.c:234
#, c-format
msgid "%s what?\n"
msgstr "%s mikä?\n"

#: src/whatis.c:378 src/whatis.c:396
#, c-format
msgid "warning: %s contains a pointer loop"
msgstr "varoitus: %s sisältää osoitinsilmukan"

#: src/whatis.c:390 src/whatis.c:398
msgid "(unknown subject)"
msgstr "(tuntematon aihe)"

#: src/whatis.c:831
#, c-format
msgid "%s: nothing appropriate.\n"
msgstr "%s: ei mitään sopivaa.\n"

#: src/zsoelim.l:183
#, c-format
msgid "%s:%d: .so requests nested too deeply or are recursive"
msgstr ""

#: src/zsoelim.l:198
#, c-format
msgid "%s:%d: warning: failed .so request"
msgstr ""

#: src/zsoelim.l:220
#, c-format
msgid "%s:%d: warning: newline in .so request, ignoring"
msgstr ""

#: src/zsoelim.l:290
#, c-format
msgid "%s:%d: warning: newline in .lf request, ignoring"
msgstr ""

#: src/zsoelim.l:330
#, c-format
msgid "%s:%d: unterminated quote in roff request"
msgstr ""

#: src/zsoelim_main.c:73
msgid "compatibility switch (ignored)"
msgstr "yhteensopivuusvalitsin (jätetään huomiotta)"

#~ msgid "must specify an input encoding"
#~ msgstr "syötteen merkistö on annettava"

#~ msgid "manpath list too long"
#~ msgstr "opastepolkujen luettelo on liian pitkä"

#, fuzzy
#~ msgid "can't restore previous working directory"
#~ msgstr "nykyistä hakemistoa ei voi määrittää"

# Kannattaako näihin jättää alkuperäinen funktion nimi vai yrittää selittää
# mitä oikeasti tarkoitetaan? "haarauttaminen epäonnistui"
#~ msgid "fork failed"
#~ msgstr "fork epäonnistui"

#~ msgid "dup2 failed"
#~ msgstr "dup2 epäonnistui"

#~ msgid "close failed"
#~ msgstr "close epäonnistui"

#~ msgid "%s: %s%s"
#~ msgstr "%s: %s%s"

#~ msgid "waitpid failed"
#~ msgstr "waitpid epäonnistui"

#~ msgid "usage: %s [-hV] [man_database]\n"
#~ msgstr "käyttö: %s [-hV] [opastetietokanta]\n"

#~ msgid "usage: %s [-dhV] [-C file] [-M manpath] [section] ...\n"
#~ msgstr "käyttö: %s [-dhV] [-C tiedosto] [-M opastepolku] [osio]...\n"

#~ msgid "usage: %s [-mcwfhV] file ...\n"
#~ msgstr "käyttö: %s [-mcwfhV] tiedosto ...\n"

#~ msgid "-m -c: incompatible options"
#~ msgstr "-m -c: epäyhteensopivat valitsimet"

#~ msgid "error trying to read from stdin"
#~ msgstr "virhe yritettäessä lukea vakiosyötteestä"

#~ msgid "error writing to temporary file %s"
#~ msgstr "virhe kirjoitettaessa väliaikaistiedostoon %s"

#~ msgid "Reformatting %s, please wait...\n"
#~ msgstr "Uudelleenmuotoillaan sivu %s, odota...\n"

#~ msgid "usage: %s [-dqspuct|-h|-V] [-C file] [-f filename] [manpath]\n"
#~ msgstr ""
#~ "käyttö: %s [-dqspuct|-h|-V] [-C tiedosto] [-f tiedostonimi] "
#~ "[opastepolku]\n"

#~ msgid "usage: %s [[-gcdq] [-C file] [-m system]] | [-V] | [-h]\n"
#~ msgstr "käyttö: %s [[-gcdq] [-C tiedosto] [-m järjestelmä]] | [-V] | [-h]\n"

#~ msgid "%s, version %s, %s\n"
#~ msgstr "%s, versio %s, %s\n"

#~ msgid ""
#~ "usage: %s [-dhV] [-r|-w|-e] [-m systems] [-M manpath] [-C file] "
#~ "keyword ...\n"
#~ msgstr ""
#~ "käyttö: %s [-dhV] [-r|-w|-e] [-m järjestelmät] [-M opastepolku] [-C "
#~ "tiedosto] avainsana ...\n"

#~ msgid ""
#~ "usage: %s [-dhV] [-r|-w] [-m systems] [-M manpath] [-C file] keyword ...\n"
#~ msgstr ""
#~ "käyttö: %s [-dhV] [-r|-w] [-m järjestelmät] [-M opastepolku] [-C "
#~ "tiedosto] avainsana ...\n"
